///刚从git贴上去
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#define STB_IMAGE_IMPLEMENTATION //让stb_image只包含函数定义
#include "stb_image.h"
#include <learnopengl/shader_m.h> //从文件中读入vs/fs来创建vs/fs
#include <filesystem> 

#include <glm/glm.hpp> //矩阵库
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//视口设置
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
std::string RUNNING_FILE_PATH = std::filesystem::current_path().string() + "\\.."; //资源绝对路径

//规格化的三角形3个顶点
float vertices[] = {
	// positions          // texture coords
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

unsigned int indices[] = { // note that we start from 0!
	0, 1, 3, // first triangle
	1, 2, 3 // second triangle
};

glm::vec3 cubePositions[] = {
	glm::vec3( 0.0f, 0.0f, 0.0f),
    glm::vec3( 2.0f, 5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3( 2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f, 3.0f, -7.5f),
    glm::vec3( 1.3f, -2.0f, -2.5f),
    glm::vec3( 1.5f, 2.0f, -2.5f),
    glm::vec3( 1.5f, 0.2f, -1.5f),
	glm::vec3(-1.3f, 1.0f, -1.5f)
};

//camera
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
//DeltaTime
float deltaTime = 0.0f; //当前帧和最后帧的时间
float lastFrame = 0.0f; //最后帧的时间

bool firstMouse = true;
float yaw   = -90.0f; //让摄像机指向z-
float pitch =  0.0f;
float lastX =  SCR_WIDTH / 2.0;
float lastY =  SCR_HEIGHT / 2.0;
float fov   =  45.0f;

//注册回调函数后，每次改变窗口，该函数都会调用
///改变大小的窗口对象
///新窗口的宽、高
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	///坐标(0, 0)屏幕左上角
	///宽，高
	glViewport(0, 0, width, height); //设置视口大小
}

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) //Esc键是否被pressed
		glfwSetWindowShouldClose(window, true);

	//控制camera
	const float cameraSpeed = 2.5f * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    	cameraPos += cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    	cameraPos -= cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    	cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
    	cameraPos += cameraSpeed * cameraUp;
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
    	cameraPos -= cameraSpeed * cameraUp;
}

//注册回调函数后，每次鼠标移动，该函数将被调用
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse) //如果是首次进入应用
    {
    	lastX = xpos;
        lastY = ypos;
    	firstMouse = false;
    }
    
    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
	lastY = ypos;

    const float sensitivity = 0.1f; //灵敏度
	xoffset *= sensitivity; 
    yoffset *= sensitivity;

    yaw += xoffset;
	pitch += yoffset;

    //限制俯仰角度
    if(pitch > 89.0f)
		pitch = 89.0f;
    if(pitch < -89.0f)
		pitch = -89.0f;

    glm::vec3 direction; //radians度数
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch)); //cos p在x轴的投影
    direction.y = sin(glm::radians(pitch));
	direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(direction);
}

//每次鼠标滚轮滚动，调用
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    fov -= (float)yoffset;
    if (fov < 1.0f)
        fov = 1.0f;
    if (fov > 45.0f)
        fov = 45.0f;
}


int main()
{
	glfwInit();
	//设置为opengl3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //设置GLFW_CONTEXT_VERSION_MAJOR为3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //设置GLFW_CONTEXT_VERSION_MINOR为3
	//使用opengl的核心子集即可
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //设置GLFW_OPENGL_PROFILE为GLFW_OPENGL_CORE_PROFILE

#ifdef __APPLE__ ///Mac系统才会用到
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //设置开启向前兼容
#endif

	///宽，高
	///窗口名称
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "OpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window); //将window的上下文作为当前线程的上下文
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback); //注册回调函数: window大小改变，视口大小也改变
	glfwSetCursorPosCallback(window, mouse_callback); //注册回调函数：鼠标移动
	glfwSetScrollCallback(window, scroll_callback); //注册回调函数：鼠标滚轮滚动

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); //告知glfw捕获鼠标，且隐藏鼠标


	//初始化GLAD
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) //glfwGetProcAddress: opengl函数指针地址
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}


	//配置全局 opengl 状态
	// -----------------------------
	glEnable(GL_DEPTH_TEST); //开启z-buffer

	///shaders
	std::string vsPath = RUNNING_FILE_PATH + "\\src\\shader.vs";
	std::string fsPath = RUNNING_FILE_PATH + "\\src\\shader.fs";
	Shader ourShader(vsPath.c_str(), fsPath.c_str());
	
	///顶点数据
	unsigned int VAO;
	glGenVertexArrays(1, &VAO); //生成VAO
	//初始化：只用设置一次
	glBindVertexArray(VAO);

	unsigned int VBO; //顶点缓存对象
	glGenBuffers(1, &VBO); //生成一个具有缓冲区ID的VBO
	//在GL_ARRAY_BUFFER的任何缓存调用将被用于配置当前绑定的缓冲区VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO); //将VBO绑定到GL_ARRAY_BUFFER
	///要将数据复制到的缓冲区的类型
	///缓存的大小
	///实际传的数据位置
	///GL_STREAM_DRAW: 数据仅被设置一次，GPU使用几次；
	///GL_STATIC_DRAW: 数据仅被设置一次，GPU多次使用；
	///GL_DYNAMIC_DRAW: 数据多次改变，GPU多次使用；
	//将先前定义的顶点数据复制到缓冲区的存储器中
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// unsigned int EBO;
	// glGenBuffers(1, &EBO); //创建EBO
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO); //绑定
	// glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW); //拷贝到GL_ELEMENT_ARRAY_BUFFER

	///set the vertex attributes pointers
	///要配置的顶点属性的位置
	///顶点属性的大小（vec3是3个值）
	///数据类型
	///数据是否需要归一化（此时暂时不用管归一化）
	///步长stride：连续顶点之间的间隔
	///Position数据在buffer中的偏移
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	//参数：顶点属性位置
	glEnableVertexAttribArray(0); //激活顶点属性

	//配置顶点属性的贴图坐标
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);  ///glVertexAttribPointer已将VBO注册为顶点属性的绑定顶点缓冲区对象，所以可解绑
	glBindVertexArray(0); ///其他调用VAO的语句已完成，可解绑


	unsigned int texture1;
	///1个Texture
	///存放在变量texture中
	glGenTextures(1, &texture1);
	glBindTexture(GL_TEXTURE_2D, texture1); //绑定该texture，之后的贴图命令都是在它上面使用
	//设置Texture包裹模式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//设置Texture放大缩小的过滤模式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int width, height, nrChannels;

	std::string aImagePath1 = RUNNING_FILE_PATH + "/resources/textures/container.jpg";
	///资源文件
	///宽
	///高
	///颜色通道
	unsigned char* data = stbi_load(aImagePath1.c_str(), &width, &height, &nrChannels, 0);
	if (data)
	{
		///在GL_TEXTURE_2D绑定的对象上生成纹理
		///指定要创建纹理的 mipmap 级别
		///以GL_RGB格式存储纹理
		///存储的宽高
		///永远是0
		///原图像的格式GL_RGBA（多了个透明度通道）
		///原图像的数据类型GL_UNSIGNED_BYTE
		///资源文件
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D); //自动为当前纹理创建 mipmaps	
	}
	else
	{
		std::cout << "Failed to load texture1" << std::endl;
	}

	stbi_image_free(data); //生成好纹理后，即可释放

	unsigned int texture2;
	///1个Texture
	///存放在变量texture中
	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2); //绑定该texture，之后的贴图命令都是在它上面使用
	//设置Texture包裹模式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//设置Texture放大缩小的过滤模式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	std::string aImagePath2 = RUNNING_FILE_PATH + "/resources/textures/awesomeface.png";

	data = stbi_load(aImagePath2.c_str(), &width, &height, &nrChannels, 0);

	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D); //自动为当前纹理创建 mipmaps	
	}
	else
	{
		std::cout << "Failed to load texture2" << std::endl;
	}

	stbi_image_free(data); //生成好纹理后，即可释放

	//分别给着色器采样器匹配texture
	ourShader.use();
	glUniform1i(glGetUniformLocation(ourShader.ID, "samTexture1"), 0); // manually 
	ourShader.setInt("samTexture2", 1); // or with shader class

	//render loop渲染循环
	while (!glfwWindowShouldClose(window))
	{
		float currentFrame = glfwGetTime(); //当前第几帧
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		///input
		processInput(window); //处理输入
        
		//////////////////////////////render//////////////////////////////////////
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		//清理深度缓存
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//清理color buffer中的值为GL_COLOR_BUFFER_BIT的值
		//GL_COLOR_BUFFER_BIT的值会被glClearColor()设置的值替代
		glClear(GL_COLOR_BUFFER_BIT); 

		//绑定贴图
		glActiveTexture(GL_TEXTURE0); 
		glBindTexture(GL_TEXTURE_2D, texture1); 
		glActiveTexture(GL_TEXTURE1); 
		glBindTexture(GL_TEXTURE_2D, texture2);

		//移动的camera
		glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		ourShader.setMat4("view", view);

		glm::mat4 projection = glm::perspective(glm::radians(fov), (float)SCR_WIDTH / SCR_HEIGHT, 0.1f, 100.0f);
		ourShader.setMat4("projection", projection); //projection

		//draw
		ourShader.use(); //激活着色器程序shaderProgram
		glBindVertexArray(VAO); ///又绑了一次？只有一个VAO其实没必要，但为了结构化

		for(unsigned int i = 0; i < 10; i++)
		{
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, cubePositions[i]); //位置移动
			float angle = 20.0f * i;
			model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f)); //旋转
			ourShader.setMat4("model", model); //改变shader的model uniform
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		///图元
		///顶点位置
		///顶点个数
		glDrawArrays(GL_TRIANGLES, 0, 36);

		///图元
		///顶点个数
		///索引数组类型
		///一般是0，除非不适用EBO
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0); //draw矩形

        //////////////////////////////render-end//////////////////////////////////////

		///检查事件、交换缓存
		glfwSwapBuffers(window); //交换颜色缓存（color-buffer保存GLFW窗口每个像素的颜色值）
		glfwPollEvents(); //检查输入事件触发
	}

	///可选: 现在出了while，没用了
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	// glDeleteBuffers(1, &EBO);

	///GLFW resources
	glfwTerminate(); //清理资源
	return 0;
}