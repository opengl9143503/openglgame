#version 330 core
out vec4 FragColor;
in vec2 TexCoord;

uniform sampler2D samTexture1;
uniform sampler2D samTexture2;

void main()
{
    ///mix函数：线性插值
    ///按第一种的80%，第二种的20%来混合
	FragColor = mix(texture(samTexture1, TexCoord), 
                    texture(samTexture2, TexCoord), 0.2);
}